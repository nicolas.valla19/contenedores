<!DOCTYPE html>
  <html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Agregar Contenedor</title>

    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-1BmE4kWBq78iYhFldvKuhfTAU6auU8tT94WrHftjDbrCEXSU1oBoqyl2QvZ6jIW3" crossorigin="anonymous">

  </head>
  <body>

  <div class="container">
  <div class="position-absolute top-50 start-50 translate-middle">
    <form action="../insertar.php" class="border p-5 form" method="POST">
        <div class="form-group">
            <label for="#">Identificacion</label>
            <input type="#" name="id" id="#" class="form-control">


        </div>
        <div class="form-group">
            <label for="#">Tara</label>
            <input type="#" name="tara" id="#" class="form-control">


        </div>
        <div class="form-group">
            <label for="#">Carga Maxima</label>
            <input type="#" name="carga_max" id="#" class="form-control">


        </div>
        <div class="form-group">
            <label for="#">Peso Bruto</label>
            <input type="#" name="peso_bruto" id="#" class="form-control">


        </div>
        <div class="form-group">
            <label for="#">Uso Frecuente</label>
            <input type="#" name="uso_frecuente" id="#" class="form-control">


        </div>
        <div class="form-group">
            <label for="#">Largo</label>
            <input type="#" name="largo" id="#" class="form-control">


        </div>
        <div class="form-group">
            <label for="#">Ancho</label>
            <input type="#" name="ancho" id="#" class="form-control">


        </div>
        <div class="form-group">
            <label for="#">Altura</label>
            <input type="#" name="altura" id="#" class="form-control">


        </div>
        <div class="form-group">
            <label for="#">Altura</label>
            <input type="#" name="capacidad" id="#" class="form-control">


        </div>
      <button type="submit" class="btn btn-primary" >Ingresar</button>
    </form>
  </div>
</div>




  <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-ka7Sk0Gln4gmtz2MlQnikT1wXgYsOg+OMhuP+IlRH9sENBO0LRn5q+8nbTov4+1p" crossorigin="anonymous"></script>
   
  </body>
  </html>